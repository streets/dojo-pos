# version 0.0.3
- Various fixes

# version 0.0.2
- Added basic Campaign support

## Notes 

- Users upgrading will need to remove both the database `/app/app.db` and the `migrations/` directory. Since there are breaking changes to the previous database models. You will lose your data.
```python
rm  app/app.db
rm -rf migrations/
flask db init
flask db migrate
flask db upgrade
```


# version 0.0.1
- Basic core functionality

## Notes 

- Upon first load the user will be asked to create an admin user. The corresponding email and password is stored in the database and is required to authenticate into the admin control panel. The password is both hashed and salted before storing.

- Once the user logs into to the admin control panel they can add a Dojo based on the .onion address and `adminkey` of the Dojo, which can both be found within the Dojo pairing code. API Key works for most calls except `/status` which is why I am currently using the `adminkey` instead of the `apikey`. I will need to explore this further.

- The user can then add an extended public key. I am currently only allowing the addition of YPUB or ZPUB extended public keys because I am having a hard time deriving BIP44 addresses. I will tackle this later and hopefully resolve the issue.
