import os
from datetime import timedelta
from app import get_env_variable
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = get_env_variable("SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

class ProdConfig(Config):
    DEBUG = False
    TESTING = False
    ASSETS_DEBUG = False

class DevConfig(Config):
    DEBUG = True
    TESTING = False
    ASSETS_DEBUG = False

class LocalConfig(Config):
    DEBUG = True
    TESTING = True
    ASSETS_DEBUG = False
