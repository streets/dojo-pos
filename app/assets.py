from flask_assets import Bundle, Environment
from app import app

bundles = {
    'common_css': Bundle(
        'vendor/bulma/css/bulma.min.css',
        Bundle(
            'styles/main.less',
            filters='less'
        ),
        filters='cssmin', output='public/css/common.css'),

    'common_js': Bundle(
        'vendor/jquery/dist/jquery.min.js',
        'vendor/@fortawesome/fontawesome-free/js/all.js',
        Bundle(
            'scripts/main.js',
            'scripts/platform.js',
            'scripts/notifications.js'
        ),
        output='public/js/common.js')
}

assets = Environment(app)
assets.register(bundles)
