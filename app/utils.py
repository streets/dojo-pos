from app.blueprints.dojo.models import Dojo
from pydojo import pyDojo
import cryptotools.BTC as bitcointools

class DojoConnection(object):
    def __init__(self):
        self.dojo_connection = Dojo.query.filter_by(is_enabled=1).first()
        if self.dojo_connection:
            self.dojo = pyDojo(self.dojo_connection.onion,self.dojo_connection.apikey)
        else:
            self.dojo = None

class AddressTools(object):
    def __init__(self, pubkey):
        self.pubkey = pubkey

        if self.pubkey.startswith("zpub"):
            self.addr_format = 'P2WPKH'
        elif self.pubkey.startswith("ypub"):
            self.addr_format = "P2WPKH-P2SH"
        else:
            self.addr_format = 'P2PKH'


    def get_addresses_from_pubkey(self, index=0, count=20):
        extended = bitcointools.Xpub.decode(self.pubkey)
        addresses = []

        for i in range(count):
            print("index: ", index)
            child = extended/0/index
            address = child.key.to_address(self.addr_format)
            addresses.append(address)
            index = index + 1
        return(addresses)
    
