(function($) {
    $(document).ready(function(){
      $('a[rel="external"]').attr('target', '_blank');

    // Check for click events on the navbar burger icon
      $(".navbar-burger").click(function() {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
      });

    });
  })(jQuery); // end of jQuery name space
