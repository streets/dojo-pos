document.addEventListener('DOMContentLoaded', () => {
    (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
      const $notification = $delete.parentNode;
  
      $delete.addEventListener('click', () => {
        $notification.parentNode.removeChild($notification);
      });
    });
  });

  function checkForPayment($payment_id) {
    var url = $SCRIPT_ROOT + '/payment/status_check'
    var payload = JSON.stringify({ payment_id: $payment_id })
    $.ajax({
      url: url,
      type: "POST",
      data: payload,
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      
      success: function(data) {
  
        if (data.paid == 1) {
          window.location = $SCRIPT_ROOT + '/payment/' + $payment_id;
        }
      }
    });    
  
  }