# Imports #
from flask import Flask, render_template
from webassets.loaders import PythonLoader as PythonAssetsLoader
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bcrypt import Bcrypt

import pyjade
import os
import jinja2

class MyApp(Flask):
    def __init__(self):
        Flask.__init__(self, __name__)
        self.jinja_loader = jinja2.ChoiceLoader([
            self.jinja_loader,
            jinja2.PrefixLoader({}, delimiter=".")
        ])

    def create_global_jinja_loader(self):
        return self.jinja_loader

    def register_blueprint(self, bp):
        Flask.register_blueprint(self, bp)
        self.jinja_loader.loaders[1].mapping[bp.name] = bp.jinja_loader

def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

app = MyApp()
app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.session_protection = "strong"
login_manager.init_app(app)


# Load config from ENV variables. 
env = (get_env_variable("SERVER"))
app.config.from_object('app.settings.%sConfig' % env.capitalize())
app.config['ENV'] = env
print('### Loading %s Config ###' % app.config['ENV'].capitalize())

# Handle Blueprints
from app.blueprints.core import core
from app.blueprints.api import api
from app.blueprints.auth import auth
from app.blueprints.dojo import dojo
from app.blueprints.payments import payments
from app.blueprints.pub import pub


app.register_blueprint(core)
app.register_blueprint(api)
app.register_blueprint(auth)
app.register_blueprint(dojo)
app.register_blueprint(payments)
app.register_blueprint(pub)


# Handle Assets
from app.assets import assets