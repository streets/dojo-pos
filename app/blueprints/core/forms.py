from flask_wtf import Form, widgets
from wtforms import TextField, StringField, TextAreaField, SelectField
from wtforms import validators
from wtforms.validators import DataRequired
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from app.blueprints.payments.models import Payment
from app.blueprints.core.models import PubKeys
from flask_login import current_user

class NewPubKey(Form):
    pubkey = StringField('public key', [validators.DataRequired()])
    label = StringField('label', [validators.DataRequired()])

class NewCampaign(Form):
    label = StringField('label', [validators.DataRequired()])
    description = TextAreaField('description')
    pubkey = QuerySelectField('pubkey', query_factory=lambda: PubKeys.query.filter_by(user_id=current_user.id),get_label='label')