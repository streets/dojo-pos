from app.blueprints.core import core
from app import app, db, login_manager

class Campaign(db.Model):
    __tablename__ = 'campaigns'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    label = db.Column(db.String(100))
    description = db.Column(db.String(512))
    is_enabled = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    pubkey_id = db.Column(db.Integer)
    payments = db.relationship('Payment', backref="campaign", cascade="all, delete-orphan" , lazy='dynamic')

    def __init__(self, label, description, pubkey_id):
        self.label = label
        self.description = description
        self.pubkey_id = pubkey_id

    def get_wallet(self):
        wallet = PubKeys.query.filter_by(id=self.pubkey_id).first() 
        return wallet

class PubKeys(db.Model):
    __tablename__ = 'pubkeys'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    pubkey = db.Column(db.String(200), unique=True)
    label = db.Column(db.String(100))
    balance = db.Column(db.String(100))
    current_index = db.Column(db.Integer, default=0)
    payments = db.relationship('Payment', backref="payments", cascade="all, delete-orphan" , lazy='dynamic')
    user_id = db.Column(db.String, db.ForeignKey('users.id'))

    def __init__(self, pubkey, label, balance, current_index):
        self.pubkey = pubkey
        self.label = label
        self.balance = balance
        self.current_index = current_index
    
    def format_btc_amount(self):
        return "{0:.8f} BTC".format(int(self.balance) * 0.00000001)

