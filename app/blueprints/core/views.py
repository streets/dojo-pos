import requests
from app.blueprints.core import core
from app import app, db
from app.blueprints.core.models import PubKeys, Campaign
from app.blueprints.payments.models import Payment
from app.blueprints.core.forms import NewPubKey, NewCampaign
from app.utils import DojoConnection, AddressTools
from flask import (session, redirect, url_for, render_template,
                   jsonify, request, send_file, json, flash, escape)
from flask_login import login_required, current_user

def get_bitcoin_price():
    url = "https://www.bitstamp.net/api/ticker/"
    resp = requests.get(url).json()
    price = float(resp['last'])
    return price

@core.route('/', methods=["GET"])
@login_required
def index():
    fiat_price = get_bitcoin_price()
    pubkeys = PubKeys.query.all()
    dojo = DojoConnection().dojo_connection
    return render_template('core/index.jade', dojo=dojo, fiat_price=fiat_price, pubkeys=pubkeys)

@core.route('/pubkeys', methods=["GET"])
@login_required
def pubkeys():
    pubkeys = PubKeys.query.all()
    pk =[]
    for pubkey in pubkeys:
        pk.append(pubkey.pubkey)

        dojo = DojoConnection().dojo_connection
        if dojo is None:
            flash("Please enable a Dojo first")
            return redirect(url_for('dojo.index'))
        else:
            dojo = DojoConnection().dojo

        xpub = dojo.get_xpub(pubkey.pubkey)

        print(xpub)
        balance = xpub["data"]["balance"]
        current_index = xpub["data"]["unused"]["external"]
        pubkey.balance = balance
        pubkey.current_index = current_index
        db.session.commit()

    return render_template('core/pubkeys.jade', pubkeys=pubkeys)

@core.route('/pubkey/<id>', methods=["GET"])
@login_required
def pubkey(id):
    pubkey = PubKeys.query.filter_by(id=id).first()
    addresses = AddressTools(pubkey.pubkey).get_addresses_from_pubkey(index=pubkey.current_index, count=20)
    
    return render_template('core/pubkey.jade', pubkey=pubkey, addresses=addresses)

@core.route('/pubkey/add', methods=["GET", "POST"])
@login_required
def add_pubkey():
    form = NewPubKey()
    if form.validate_on_submit():
        pub_key = form.pubkey.data
        label = form.label.data
        pubkey = PubKeys.query.filter_by(pubkey=pub_key).first()
        if not pubkey:
            
            if str(pub_key).lower().startswith("xpub"):
                flash("Please only add a YPUB or ZPUB")
                return redirect(url_for('core.add_pubkey'))

            elif str(pub_key).lower().startswith("ypub"):
                segwit = True
                derivation = "bip49"
            elif str(pub_key).lower().startswith("zpub"):
                segwit = True
                derivation = "bip84"

            
            dojo = DojoConnection().dojo

            if segwit is True:
                dojo.add_xpub(pub_key, type="restore", segwit=derivation)
            else:
                dojo.add_xpub(pub_key, type="restore")

            xpub = dojo.get_xpub(pub_key)
            print(xpub)
            balance = xpub["data"]["balance"]
            current_index = xpub["data"]["unused"]["external"]
            new_pubkey = PubKeys(pubkey=pub_key, label=label, balance=balance, current_index=current_index)
            user = current_user
            user.pubkeys.append(new_pubkey)
            db.session.commit()
            flash("Added Public Key")
            return redirect(url_for('core.pubkeys'))
        else:
            flash("Public key already exists.")
            return redirect(url_for('core.add_pubkey'))

    return render_template('core/add_pubkey.jade', form=form)

@core.route('/pubkey/<id>/remove', methods=["GET"])
@login_required
def remove_pubkey(id):
    pubkey = PubKeys.query.filter_by(id=id).first()
    if pubkey:
        db.session.delete(pubkey)
        db.session.commit()
        flash("Removed Pubkey")
        return redirect(url_for('core.pubkeys'))

@core.route('/campaigns', methods=["GET"])
@login_required
def campaigns():
    campaigns = Campaign.query.all()
    return render_template('core/campaigns.jade', campaigns=campaigns)

@core.route('/campaign/add', methods=["GET", "POST"])
@login_required
def add_campaign():

    pubkeys = PubKeys.query.count()
    if pubkeys == 0:
        flash("Please add a wallet first")
        return redirect(url_for('core.add_pubkey'))

    form = NewCampaign()
    if form.validate_on_submit():
        label = form.label.data
        description = form.description.data
        pubkey_id = int(request.form.get('pubkey'))

        new_campaign = Campaign(label=label, description=description, pubkey_id=pubkey_id)
        user = current_user
        user.campaigns.append(new_campaign)
        db.session.commit()
        flash("Added Campaign")
        return redirect(url_for('core.campaigns'))

    return render_template('core/add_campaign.jade', form=form)

@core.route('/campaign/<id>', methods=["GET"])
@login_required
def campaign(id):
    campaign = Campaign.query.filter_by(id=id).first()
    
    return render_template('core/campaign.jade', campaign=campaign)

@core.route('/campaign/<id>/enable', methods=["GET"])
@login_required
def enable_campaign(id):
    campaign = Campaign.query.filter_by(id=id).first()
    if campaign:

        campaign.is_enabled = 1
        db.session.add(campaign)
        db.session.commit()
        
        return redirect(url_for('core.campaigns'))

@core.route('/campaign/<id>/disable', methods=["GET"])
@login_required
def disable_campaign(id):
    campaign = Campaign.query.filter_by(id=id).first()
    if campaign:
        campaign.is_enabled = 0
        db.session.add(campaign)
        db.session.commit()
        return redirect(url_for('core.campaigns'))

@core.route('/campaign/<id>/remove', methods=["GET"])
@login_required
def remove_campaign(id):
    campaign = Campaign.query.filter_by(id=id).first()
    if campaign:
        db.session.delete(campaign)
        db.session.commit()
        return redirect(url_for('core.campaigns'))

@core.route('/campaign/<id>/payment', methods=["GET"])
def campaign_payment(id):
    campaign = Campaign.query.filter_by(id=id).first()
    if campaign:

        pubkey = PubKeys.query.filter_by(id=campaign.pubkey_id).first()

        if pubkey:
            index = pubkey.current_index

            while Payment.query.filter_by(addr_index=index).count() > 0:
                index = index+1

            address = AddressTools(pubkey.pubkey).get_addresses_from_pubkey(index=index, count=1)[0]
            payment_id = Payment.make_unique_pid()
            amount = 0
            status = 0
            payment = Payment(address=address, addr_index=index, payment_id=payment_id, amount=amount, status=status)
            pubkey.payments.append(payment)
            campaign.payments.append(payment)
            db.session.commit()
    return redirect(url_for('payments.payment', payment_id=payment_id))
