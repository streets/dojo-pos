from app import app, db
import uuid, shortuuid
from datetime import datetime
from app.blueprints.core.models import PubKeys, Campaign
class Payment(db.Model):
    __tablename__ = 'payments'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    address = db.Column(db.String(120), unique=True,nullable=False)
    addr_index = db.Column(db.Integer, default=0)
    pubkey_id = db.Column(db.Integer, db.ForeignKey('pubkeys.id'))
    campaign_id = db.Column(db.Integer, db.ForeignKey('campaigns.id'))
    payment_id = db.Column(db.String(64), unique=True)
    tx_hash = db.Column(db.String(256), unique=True)
    amount = db.Column(db.String(100))
    status = db.Column(db.Integer, default=0)
    request_date = db.Column(db.DateTime, default=datetime.now)
    received_date = db.Column(db.DateTime)

    @staticmethod
    def make_unique_pid():
        u = uuid.uuid4()
        s = shortuuid.encode(u)
        return str(s)

    def get_wallet(self):
        wallet = PubKeys.query.filter_by(id=self.pubkey_id).first() 
        return wallet

    def get_campaign(self):
        campaign = Campaign.query.filter_by(id=self.campaign_id).first() 
        return campaign

    def format_btc_amount(self):
        return "{0:.8f} BTC".format(int(self.amount) * 0.00000001)