from app.blueprints.payments import payments
from app import app, db
from app.blueprints.payments.models import Payment
from app.blueprints.core.models import PubKeys
from app.utils import DojoConnection, AddressTools
from flask import (session, redirect, url_for, render_template,
                   jsonify, request, send_file, json, flash, escape)
from flask_login import login_required

from datetime import datetime
from flask_qrcode import QRcode
qrcode = QRcode(app)


@payments.route('/payments', methods=["GET"])
@login_required
def index():
    payments = Payment.query.all()
    return render_template('payments/index.jade', payments=payments)

@payments.route('/donate', methods=["GET"])
def donate():
    pubkey = PubKeys.query.first()
    index = pubkey.current_index

    while Payment.query.filter_by(addr_index=index).count() > 0:
        index = index+1

    address = AddressTools(pubkey.pubkey).get_addresses_from_pubkey(index=index, count=1)[0]
    payment_id = Payment.make_unique_pid()
    amount = 0
    status = 0
    payment = Payment(address=address, addr_index=index, payment_id=payment_id, amount=amount, status=status)
    pubkey.payments.append(payment)
    db.session.commit()
    return redirect(url_for('payments.payment', payment_id=payment_id))

@payments.route('/payment/<string:payment_id>', methods=["GET"])
def payment(payment_id):
    payment = Payment.query.filter_by(payment_id=payment_id).first()
    return render_template('payments/payment.jade', payment=payment)

@payments.route('/payment/<string:payment_id>/remove', methods=["GET"])
def remove(payment_id):
    payment = Payment.query.filter_by(payment_id=payment_id).first()
    if payment:
        db.session.delete(payment)
        db.session.commit()
        flash("Removed Payment")
        return redirect(url_for('pub.cancelled'))

@payments.route('/payment/status_check', methods=["POST"])
def purchase_status_check():
    response = {}
    if not request.json:
        rcode = 400
        response['message'] = '400 - Bad Request'
        return jsonify(**response), rcode

    if "payment_id" not in request.json:
        rcode = 400
        response['message'] = '400 - Bad Request'
        return jsonify(**response), rcode
    
    payment_id = request.json['payment_id']
    payment = Payment.query.filter_by(payment_id=payment_id).first()

    if not payment:
        rcode = 400
        response['message'] = '400 - Bad Request'
        return jsonify(**response), rcode

    dojo = DojoConnection().dojo
    payment_data = dojo.get_wallet([payment.address])
    print(payment_data)

    if not payment_data["txs"]:
        payment_status = False
    else:
        for tx in payment_data["txs"]:
            for utxo in tx["out"]:
                if utxo["addr"] == payment.address:
                    payment.tx_hash = tx["hash"]
                    payment.status = 1
                    payment.received_date = datetime.fromtimestamp(tx["time"])
                    payment.amount = utxo["value"]
                    db.session.commit()
                    payment_status = True

    if payment_status == False:
        return jsonify(paid=0)
    
    elif payment_status == True:
        return jsonify(paid=1)
    
    elif payment_status == None:
        return jsonify(paid=0)