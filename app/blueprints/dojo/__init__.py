from flask import Blueprint

dojo = Blueprint(
  'dojo', __name__,
  template_folder='templates',
  static_folder='static'
)

from app.blueprints.dojo import views