from app import app, db

class Dojo(db.Model):
    __tablename__ = 'dojos'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    onion = db.Column(db.String(120), unique=True,nullable=False)
    apikey = db.Column(db.String(120), unique=True, nullable=False)
    is_enabled = db.Column(db.Boolean, default=False)

    def __init__(self, onion, apikey):
        self.onion = onion
        self.apikey = apikey
