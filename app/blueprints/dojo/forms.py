from flask_wtf import Form, widgets
from wtforms import TextField, StringField, TextAreaField, PasswordField
from wtforms import validators
from wtforms.validators import DataRequired

class NewDojo(Form):
    onion = StringField('onion', [validators.DataRequired()])
    api_key = StringField('api_key', [validators.DataRequired()])
