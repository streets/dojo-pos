from app.blueprints.dojo import dojo
from app.blueprints.dojo.forms import NewDojo
from app.blueprints.dojo.models import Dojo
from app import app, db

from flask import (session, redirect, url_for, render_template,
                   jsonify, request, send_file, json, flash, escape)

from flask_login import login_required
from pydojo import pyDojo
import os

@dojo.route('/dojo', methods=["GET"])
@login_required
def index():
    status = None
    dojo_connections = Dojo.query.all()
    if dojo_connections:
        for dojo in dojo_connections:
            if dojo.is_enabled == 1:
                status = pyDojo(dojo.onion,dojo.apikey).get_status()

    return render_template('dojo/index.jade', dojo_connections=dojo_connections, status=status)

@dojo.route('/dojo/<id>/remove', methods=["GET"])
@login_required
def remove(id):
    dojo = Dojo.query.filter_by(id=id).first()
    if dojo:
        db.session.delete(dojo)
        db.session.commit()
        os.remove("access_token.pickle")
        flash("Removed Dojo")
        return redirect(url_for('dojo.index'))

@dojo.route('/dojo/<id>/enable', methods=["GET"])
@login_required
def enable(id):
    existing_enabled = Dojo.query.filter_by(is_enabled=1).all()
    dojo = Dojo.query.filter_by(id=id).first()
    if dojo:
        if existing_enabled:
            for d in existing_enabled:
                d.is_enabled = 0
                db.session.add(d)
                db.session.commit()
                os.remove("access_token.pickle")

        dojo.is_enabled = 1
        db.session.add(dojo)
        db.session.commit()
        
        return redirect(url_for('dojo.index'))

@dojo.route('/dojo/<id>/disable', methods=["GET"])
@login_required
def disable(id):
    dojo = Dojo.query.filter_by(id=id).first()
    if dojo:
        dojo.is_enabled = 0
        os.remove("access_token.pickle")
        db.session.add(dojo)
        db.session.commit()
        return redirect(url_for('dojo.index'))

@dojo.route('/dojo/add', methods=["GET", "POST"])
@login_required
def add():
    form = NewDojo()
    if form.validate_on_submit():
        onion = form.onion.data
        api_key = form.api_key.data

        dojo = Dojo.query.filter_by(onion=onion).first()

        if not dojo:
            new_dojo = Dojo(onion=onion, apikey=api_key)
            db.session.add(new_dojo)
            db.session.commit()
            flash("Added Dojo")
            return redirect(url_for('dojo.index'))
        else:
            flash("Dojo already exists.")
            return redirect(url_for('dojo.add'))

    else:
        return render_template('dojo/add.jade', form=form)
