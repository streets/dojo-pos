from app.blueprints.auth import auth
from app.blueprints.auth.forms import LoginForm
from app.blueprints.auth.models import User
from app import app, db
from flask import redirect, url_for, render_template, flash
from flask_login import (current_user, login_user, logout_user, 
                        login_required)

@auth.before_request
def before_request():
    if current_user.is_authenticated:
        pass

@auth.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm()
    users = User.query.all()
    if not users:
        flash("Setup instance")
        return redirect(url_for('pub.setup'))
    else:
        if form.validate_on_submit():
            email_address = form.email_address.data
            password = form.password.data
            user = User.query.filter_by(email_address=email_address).first()

            if user and user.verify_password(password):
                login_user(user)
                flash("User Logged In")
                return redirect(url_for('core.index')) 
            else:
                flash("Invalid Login")
                return redirect(url_for('auth.login')) 

        return render_template('auth/login.jade', form=form)

@auth.route('/logout', methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
