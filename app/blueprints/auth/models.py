from flask_login import UserMixin
from app import app, db, login_manager, bcrypt

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True)
    email_address = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100))
    role = db.Column(db.Integer, default=0)
    pubkeys = db.relationship('PubKeys', backref="user", cascade="all, delete-orphan" , lazy='dynamic')
    campaigns = db.relationship('Campaign', backref="user", cascade="all, delete-orphan" , lazy='dynamic')

    def __init__(self, email_address, password, name, role):
        self.email_address = email_address
        self.password = bcrypt.generate_password_hash(password).decode('UTF-8')
        self.name = name
        self.email_address = email_address
        self.role = role

    def verify_password(self, password):
        return bcrypt.check_password_hash(self.password, password)
    
    def is_super_admin(self):
        if self.role == 9:
            return True