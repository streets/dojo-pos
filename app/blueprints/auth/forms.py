from flask_wtf import Form, widgets
from wtforms import TextField, StringField, TextAreaField, PasswordField
from wtforms import validators
from wtforms.validators import DataRequired

class LoginForm(Form):
    email_address = StringField('email', [validators.DataRequired()])
    password = PasswordField('password', [validators.DataRequired()])
