from app.blueprints.pub import pub
from app.blueprints.pub.forms import SetupForm
from app.blueprints.auth.models import User
from app import app, db
from app.utils import DojoConnection, AddressTools
from flask import (session, redirect, url_for, render_template,
                   jsonify, request, send_file, json, flash, escape)

@pub.route('/setup', methods=["GET", "POST"])
def setup():
    '''
    Run only after a freshly initalized instance of Dojo-POS. If a user
    entry exists in the database the user is redirected to login instead.
    '''
    users = User.query.all()
    if users:
        flash("Instance already setup")
        return redirect(url_for('core.index'))
    else:
        form = SetupForm()
        if form.validate_on_submit():
            email_address = form.email_address.data
            name = form.name.data
            password = form.password.data
            role = 9 # super admin

            user = User.query.filter_by(email_address=email_address).first()

            if not user:
                new_user = User(email_address=email_address, name=name, password=password, role=role)
                db.session.add(new_user)
                db.session.commit()
                return redirect(url_for('auth.login'))
        else:
            return render_template('auth/setup.jade', form=form)

@pub.route('/cancelled', methods=["GET"])
def cancelled():
    '''
    Public page that is returned to users who choose to cancel a payment in progress
    '''
    return render_template('pub/cancelled.jade')
