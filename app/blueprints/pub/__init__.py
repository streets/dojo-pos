from flask import Blueprint

pub = Blueprint(
  'pub', __name__,
  template_folder='templates',
  static_folder='static'
)

from app.blueprints.pub import views