
# Summary

Dojo-POS is a serverside application intended to run on a personal PC, RoninDojo, or cheap VPS and interface with an existing Dojo API server. In development all calls made to the API are made through a SOCKS proxy to the Tor V3 onion address of a test RoninDojo device, but it is hosted locally on my development machine not on the RoninDojo device. No optimizations have yet been made based on that device.

# Current Status

- ✅ Create admin user
- ✅ Add one or many Dojo instances (only one enabled at a time)
- ✅ Add one or many `YPUB` or `ZPUB` extended public keys
- ✅ Generate a new payment address dervied from the provided extended public keys
- ✅ Monitor for payment received on that payment address
- ✅ Create user(s) and roles. (Roles implemented but there is still only one role(admin) and there is no UI for adding new users)
- ✅ Create or many "campaigns"
- (Todo) Add a simple REST API
- (Todo) Generate embedable "widgets" for easy sharing
- (Todo) Invoice management (create, edit, delete, etc..) 
- (Todo) Optional domain name management and SSL provision/management

# Get Started

## Pre Install

Install and usage instructions assume you are running Ubuntu 20.04. Any linux varient should work the same. Modify/tweak as needed.

### Install pip

    sudo apt install python3-pip

### Install virtualenv

    sudo pip3 install virtualenv

### Install yarn
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt update
    sudo apt install yarn
    yarn --version

### Install less
    sudo apt install node-less
    
## Install

### Download the code
    git clone https://gitlab.com/streets/dojo-pos.git
    cd dojo-pos/
    
### Create Virtual Env
    virtualenv --python=/usr/bin/python3 venv

### Open `venv/bin/activate` with nano
    nano venv/bin/activate

### Add ENV variables to the end of the file
    export FLASK_APP=run.py
    export SERVER=local
    export SECRET_KEY=<some_good_secret_key>

### Activate Virtual Env
    source venv/bin/activate

### Install Requirements
    pip install -r requirements.txt

### Install Assets
    yarn install

### Create Database
    flask db init
    flask db migrate
    flask db upgrade

### Run development server
    flask run

## Launch Dojo-POS
Navigate to `127.0.0.1:5000` to launch the admin control panel
